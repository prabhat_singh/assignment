#Assignment Application 

The assignment application is implemented in order to perform CRUD operations with a User entity.

###Installation

The project is created with Maven, so you just need to import it to your IDE and build the project to resolve the dependencies.

###Database configuration

In this project H2 in memory database is used. you can access h2's console in your browser at http://localhost:9090/assignment/h2-console and then click connect to use the h2 console.
if want credentials can be added to /resources/application.properties.
the default ones are : 

spring.datasource.url=jdbc:h2:~/test;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.username=sa
spring.datasource.password=
spring.datasource.driverClassName=org.h2.Driver
spring.jpa.hibernate.ddl-auto=update
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.H2Dialect

###Credentials
All api are secure with spring security BASIC authentication. So to access any of them through POSTMAN use
Authorization : Basic cHJhYmhhdDpwcmFiaGF0 
In header or you can generate Token by selecting Basic Auth in type dropdown and provide 
Username : prabhat
Password : prabhat
that will add authorization in header with your request payload.

###Swagger-ui
This project is integrated with swagger ui which is an open source project to visually render documentation for an API defined with the OpenAPI Specification.
The swagger html page can be access by the URL http://localhost:9090/assignment/swagger-ui.html in the browser.

###Usage 

Run the project through the IDE 

or

Run this command in the command line:

mvn clean spring-boot:run 

And then head out to http://localhost:9090/assignment. you need to provide
Username : prabhat
password : prabhat

###Running the test

Run the test through the IDE

or

Run this command in the command line:

mvn clean test 