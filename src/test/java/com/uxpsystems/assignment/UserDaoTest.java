package com.uxpsystems.assignment;

import com.uxpsystems.assignment.beans.Status;
import com.uxpsystems.assignment.beans.User;
import com.uxpsystems.assignment.dao.UserDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserDaoTest {

    @Autowired
    private UserDao userDao;

    User user = new User("prabhat", "prabhat", Status.Activated);

    @Before
    public void saveUser() {
        userDao.save(user);
    }

    @Test
    public void updateUserTest() {
        user.setStatus(Status.Deactivated);
        User user1 = userDao.save(user);
        Assert.assertEquals(user.getStatus(), user1.getStatus());
    }

    @Test(expected = JpaObjectRetrievalFailureException.class)
    public void deleteUserTest() {
        userDao.delete(user);
        userDao.getOne(user.getId());
    }

    @Test(expected = JpaObjectRetrievalFailureException.class)
    public void deleteUserByIdTest() {
        userDao.delete(user.getId());
        userDao.getOne(user.getId());
    }

    @Test
    public void getOneByIdTest() {
        User user1 = userDao.getOne(user.getId());
        Assert.assertEquals(user, user1);
    }

    @Test
    public void findAllTest(){
        List<User> userList = userDao.findAll();
        Assert.assertEquals(Arrays.asList(user), userList);
    }
}