package com.uxpsystems.assignment;

import com.uxpsystems.assignment.beans.Status;
import com.uxpsystems.assignment.beans.User;
import com.uxpsystems.assignment.dao.UserDao;
import com.uxpsystems.assignment.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
public class UserServiceTest {

    @TestConfiguration
    static class UserServiceTestContextConfiguration {

        @Bean
        public UserService userService(){
            return new UserService();
        }
    }

    @Autowired
    private UserService userService;

    @MockBean
    private UserDao userDao;

    User user = new User("prabhat","prabhat", Status.Activated);

    @Before
    public void setUp() {
        Mockito.when(userDao.save(user)).thenReturn(user);
        Mockito.when(userDao.getOne(user.getId()))
                .thenReturn(user);
        Mockito.when(userDao.findAll())
                .thenReturn(Arrays.asList(user));
        Mockito.when(userDao.exists(user.getId())).thenReturn(false);
    }

    @Test
    public void saveUserTest(){
        User testUser = userService.save(user);
        Assert.assertEquals(user,testUser);
    }

    @Test
    public void updateUserTest(){
        user.setUsername("prabhat1");
        Mockito.when(userDao.save(user)).thenReturn(user);
        User testUser = userService.save(user);
        Assert.assertEquals(user,testUser);
    }

    @Test
    public void getOneByIdTest(){
        User testUser = userService.getOneById(user.getId());
        Assert.assertEquals(user,testUser);
    }

    @Test
    public void findAllTest(){
        List<User> userList = userService.findAll();
        Assert.assertEquals(Arrays.asList(user),userList);
    }

}
