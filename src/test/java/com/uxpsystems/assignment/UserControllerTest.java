package com.uxpsystems.assignment;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uxpsystems.assignment.beans.Status;
import com.uxpsystems.assignment.beans.User;
import com.uxpsystems.assignment.controller.UserController;
import com.uxpsystems.assignment.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    private ObjectMapper mapper = new ObjectMapper();

    @Test
    public void createEntityTest() throws Exception {
        User user = new User("prabhat", "prabhat", Status.Activated);

        String requestContent = mapper.writeValueAsString(user);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/user/create")
                .header("authorization", "Basic cHJhYmhhdDpwcmFiaGF0")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isCreated());
    }

    @Test
    public void updateEntityTest() throws Exception {
        User user = new User("prabhat", "prabhat", Status.Activated);

        String requestContent = mapper.writeValueAsString(user);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put("/user/update")
                .header("authorization", "Basic cHJhYmhhdDpwcmFiaGF0")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk());

    }

    @Test
    public void deleteEntityTest() throws Exception{
        User user = new User("prabhat", "prabhat", Status.Activated);

        String requestContent = mapper.writeValueAsString(user);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .delete("/user/delete")
                .header("authorization", "Basic cHJhYmhhdDpwcmFiaGF0")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk());
    }

    @Test
    public void findAllTest() throws Exception {
        User user = new User("prabhat", "prabhat", Status.Activated);

        List<User> userList = Arrays.asList(user);

        given(userService.findAll()).willReturn(userList);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/user/list/all")
                .header("authorization", "Basic cHJhYmhhdDpwcmFiaGF0")
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().json("[{'username':'prabhat', 'password':'prabhat', 'status':'Activated'}]"));
    }
}
