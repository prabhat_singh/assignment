package com.uxpsystems.assignment.service;

import com.uxpsystems.assignment.beans.User;
import com.uxpsystems.assignment.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Prabhat Singh
 * @Since: 1/3/19
 */
@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public User save(User user) {
        return userDao.save(user);
    }

    public void delete(User user) {
        userDao.delete(user);
    }

    public void deleteById(Long id) {
        userDao.delete(id);
    }

    public User getOneById(Long id) {
        try {
            return userDao.getOne(id);
        } catch (Exception e) {
            return null;
        }
    }

    public List<User> findAll() {
        try {
            return userDao.findAll();
        } catch (Exception e) {
            return null;
        }
    }
}
