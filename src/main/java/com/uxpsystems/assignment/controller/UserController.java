package com.uxpsystems.assignment.controller;

import com.uxpsystems.assignment.beans.User;
import com.uxpsystems.assignment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Prabhat Singh
 * @Since: 1/3/19
 */
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createEntity(@Valid @RequestBody User entity) {
        User user = userService.save(entity);
        return new ResponseEntity(user, HttpStatus.CREATED);
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateEntity(@Valid @RequestBody User entity) {
        User user = userService.save(entity);
        return new ResponseEntity(user, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deleteEntity(@Valid @RequestBody User entity) {
        userService.delete(entity);
        return new ResponseEntity(null, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity deleteEntityById(@NotNull @Validated @PathVariable("id") Long id) {
        userService.deleteById(id);
        return new ResponseEntity(null, HttpStatus.OK);
    }

    @GetMapping(value = "/find/{id}")
    public ResponseEntity findById(@NotNull @Validated @PathVariable("id") Long id) {
        User user = userService.getOneById(id);
        return new ResponseEntity(user, HttpStatus.OK);
    }

    @GetMapping(value = "/list/all")
    public ResponseEntity findAll() {
        List<User> userList = userService.findAll();
        return new ResponseEntity(userList, HttpStatus.OK);
    }

}
