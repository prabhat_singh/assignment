package com.uxpsystems.assignment.controller;

import com.uxpsystems.assignment.beans.ExceptionResponse;
import org.hibernate.TypeMismatchException;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.naming.ServiceUnavailableException;
import java.net.BindException;
import java.nio.file.AccessDeniedException;
import java.sql.SQLException;

@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * Convert a predefined exception to an HTTP Status code
     */
    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity conflict() {
        ExceptionResponse error = new ExceptionResponse(HttpStatus.CONFLICT.value(),
                HttpStatus.CONFLICT.getReasonPhrase(), "Data integrity violation.");
        return new ResponseEntity(error, HttpStatus.CONFLICT);
    }

    /**
     * Convert a predefined exception to an HTTP Status code and specify the
     * name of a specific view that will be used to display the error.
     * Return value '400' used as logical view name
     * of an error page, passed to view-resolver(s) in usual way.
     * @return ExceptionResponse
     */
    @ExceptionHandler({BindException.class, HttpMessageNotReadableException.class, HttpRequestMethodNotSupportedException.class,
            MethodArgumentNotValidException.class, MissingServletRequestParameterException.class,
            MissingServletRequestPartException.class, TypeMismatchException.class})
    public ResponseEntity badRequestError(Exception e) {
        ExceptionResponse error = new ExceptionResponse(HttpStatus.BAD_REQUEST.value(),
                HttpStatus.BAD_REQUEST.getReasonPhrase(), e.getLocalizedMessage());
        return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity forbiddenError(Exception e) {
        ExceptionResponse error = new ExceptionResponse(HttpStatus.FORBIDDEN.value(),
                HttpStatus.FORBIDDEN.getReasonPhrase(), e.getLocalizedMessage());
        return new ResponseEntity(error, HttpStatus.FORBIDDEN);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity notFoundError(Exception e) {
        ExceptionResponse error = new ExceptionResponse(HttpStatus.NOT_FOUND.value(),
                HttpStatus.NOT_FOUND.getReasonPhrase(), e.getLocalizedMessage());
        return new ResponseEntity(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ConversionNotSupportedException.class, HttpMessageNotWritableException.class,
            MissingPathVariableException.class, SQLException.class, DataAccessException.class,
            NullPointerException.class})
    public ResponseEntity internalServerError(Exception e) {
        ExceptionResponse error = new ExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), e.getLocalizedMessage());
        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({ServiceUnavailableException.class})
    public ResponseEntity serviceUnavailableError(Exception e) {
        ExceptionResponse error = new ExceptionResponse(HttpStatus.SERVICE_UNAVAILABLE.value(),
                HttpStatus.SERVICE_UNAVAILABLE.getReasonPhrase(), e.getLocalizedMessage());
        return new ResponseEntity(error, HttpStatus.SERVICE_UNAVAILABLE);
    }

    /**
     * If the exception is annotated with @ResponseStatus rethrow it and let the framework handle it
     * like the DataIntegrityViolationException example at the start of this post.
     * AnnotationUtils is a Spring Framework utility class.
     * Otherwise setup and send the user to a default error-view.
     * @param e
     * @return ExceptionResponse
     * @throws Exception
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity defaultErrorHandler(Exception e) throws Exception {
        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)	throw e;
        ExceptionResponse error = new ExceptionResponse(500, e.getMessage(), e.getLocalizedMessage());
        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
