package com.uxpsystems.assignment.beans;

/**
 * @author Prabhat Singh
 * @Date: 1/3/19
 */

public enum Status {
    Activated,
    Deactivated
}
