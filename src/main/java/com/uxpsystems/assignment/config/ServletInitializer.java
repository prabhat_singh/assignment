package com.uxpsystems.assignment.config;

import com.uxpsystems.assignment.AssignmentApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * @author Prabhat Singh
 * @Since: 1/3/19
 */

public class ServletInitializer extends SpringBootServletInitializer{
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder applicationBuilder){
        return applicationBuilder.sources(AssignmentApplication.class);
    }
}
