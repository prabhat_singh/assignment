package com.uxpsystems.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Prabhat Singh
 * @Since: 1/3/19
 */
@SpringBootApplication
public class AssignmentApplication {
    public static void main(String[] args){
        SpringApplication.run(AssignmentApplication.class, args);
    }
}
