package com.uxpsystems.assignment.dao;

import com.uxpsystems.assignment.beans.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Prabhat Singh
 * @Date: 1/3/19
 */
@Transactional
public interface UserDao extends JpaRepository<User, Long> {
}
